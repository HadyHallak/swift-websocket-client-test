//
//  ViewController.swift
//  websocket-test
//
//  Created by Hady Hallak on 23.12.2018.
//  Copyright © 2018 TamTek. All rights reserved.
//

import UIKit
import Starscream

class ViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var logLabel: UILabel!
    
    var log = "" {
        didSet {
            logLabel.text = log
        }
    }
    
    let socket = WebSocket(url: URL(string: "wss://websockettestproject20181220045804.azurewebsites.net/chat")!)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        socket.delegate = self
        socket.connect()
    }

    @IBAction func send(_ sender: Any) {
        print(socket.isConnected)
        
        if let text = textField.text {
            if !text.isEmpty {
                socket.write(string: text)
                textField.text = nil
            }
        }
    }
    
    deinit {
        socket.disconnect()
        socket.delegate = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ViewController : WebSocketDelegate {
    
    func websocketDidConnect(socket: WebSocketClient) {
        log.append("\n")
        log.append("connected to socket successfully.")
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        log.append("\nsocket disconnected!")
        if error != nil {
            log.append(" - Reason: " + error!.localizedDescription)
        }
        
        socket.connect()
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        log.append("\n")
        log.append(text)
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        if let text = String(data: data, encoding: .utf8) {
            log.append("\n")
            log.append(text)
        }
    }
}


